window.onload = iniciar;
/*Funcion que inicia cuando la pagina carga completamente*/
function iniciar(){

	/*Se captura una referencia del boton enviar*/
	boton = document.getElementById("btn_enviar");

	/*Al dar click en el boton, se ejecuta la función validar*/
	boton.onclick = validar;	
}

function validar(){
		/*Captura las referencia de los inputs y del h1*/
		var nom = document.getElementById('nombre');
		var email = document.getElementById('email');
		var tel = document.getElementById('telefono');
		var title = document.getElementById('titulo');

		/*Valida si el contenido de los inputs está vacío 
		y envía una notificación de errror*/
		if(nom.value == "" || email.value == "" || tel.value == ""){
			alertify.error("Por favor complete todos los campos");
		}
		else{
			document.datos.reset();

			/*Muestra una ventana con un mensaje*/
			alertify.alert('Formulario completo','Muchas gracias por participar en la encuesta').set('label', 'Alright!') 
				.setting({
					'labels':{ok:'Yeah!'},
					'message': 'Muchas gracias por participar en la encuesta',

					/*funcion que se ejecuta al dar click al boton de la ventana*/
					'onok':function(){
						
						/*Agrega las clases necesarias para animar los inputs,boton, y titulo*/
						alertify.success('Harlem Shake');
						title.className ='shake shake-slow shake-constant fijar';
						nom.className ='shake shake-rotate shake-constant fijar';
						email.className ='shake shake-vertical shake-constant fijar';
						tel.className ='shake shake-opacity shake-constant fijar';
						boton.classList.add("shake","shake-constant","fijar");
					}
				}).show();
		}
		
}